export interface WidgetModel {
    name: string;
    identifier: string;
}

export interface DashboardContentModel {
    cols: number;
    rows: number;
    y: number;
    x: number;
    component?: any;
    name: string;
    componentName: string;
}

export interface DashboardModel {
    id: number;
    username: string;
    dashboard: Array<DashboardContentModel>;
}

export const WidgetsMock: WidgetModel[] = [
    
]