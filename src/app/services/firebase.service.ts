import { Injectable } from '@angular/core';

import { AngularFirestore, DocumentSnapshot } from '@angular/fire/firestore';

import { DashboardModel } from '../../models/dashboard.model';
import { DashboardContentModel } from '../../models/dashboard.model'
import { Observable } from 'rxjs';
@Injectable()
export class FirebaseService {

  docId: string = "Hl6jCUbVwizM5PdlkM3c";
  collectionUserDashboard: string = 'UserDashboards';

  constructor(private _firestore: AngularFirestore) { }

  insertWidget(widget: DashboardContentModel) {
    return this._firestore.collection(this.collectionUserDashboard).doc(this.docId).set(widget);
  }

  getDashboard(id: number): Observable<DocumentSnapshot<any>> {
    const userDashboardRef = this._firestore.collection(this.collectionUserDashboard).doc(this.docId);

    return userDashboardRef.get() as Observable<DocumentSnapshot<any>>;
  }

  updateDashboard(dashboard: DashboardModel) {
    return this._firestore.collection(this.collectionUserDashboard).doc(this.docId).set(dashboard);
  }


  //----------------- DashboardModel


  upsertDashboardModel(dm: DashboardModel) {
    console.log("dm: ", dm)

    let copyDm: any = this.clone2(dm)
    console.log("dm:  ,", dm)
    //  dm.dashboard.forEach(c=>c.component=null)
    return this._firestore.collection(this.collectionUserDashboard).doc(this.docId).set(copyDm);

  }

  clone1(obj) {
    let clone = Object.assign({}, obj)
    clone.dashboard = obj.dashboard.slice();
    console.log("copyDm: ", clone)
    clone.dashboard.forEach(c => {
      console.log(c)
      c.component = null
    })
    return clone
  }

  clone2(obj) {
    let clone = JSON.parse(JSON.stringify(obj));
    console.log("copyDm: ", clone)
    clone.dashboard.forEach(c => {
      console.log(c)
      c.component = null
    })
    return clone
  }
}

