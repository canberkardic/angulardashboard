import { WidgetModel, DashboardModel } from '../../models/dashboard.model';
import { Injectable, ComponentFactoryResolver } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { IToolboxComponent, ToolboxComponent } from '../components/toolbox-decorator';



@Injectable({
  providedIn: "root"
})
export class DashboardService {
  constructor(private _http: HttpClient,
    private _resolver: ComponentFactoryResolver) { }

  getToolboxWidgets(): IToolboxComponent[] {
    var factories = Array.from(this._resolver['_factories'].keys());
    const comps = factories
      .filter(f => f.hasOwnProperty(ToolboxComponent.name))
      .map((fact: any) => fact[ToolboxComponent.name])
    return comps;
  }

  getToolboxWidget(componentName: String): IToolboxComponent {
    const bulunan = this.getToolboxWidgets()
      .find((f: IToolboxComponent) =>  f.componentName === componentName);
    return bulunan;
  }

  // Return Array of DashboardModel
  getDashboards(): Observable<Array<DashboardModel>> {
    return this._http.get<Array<DashboardModel>>(`https://Node-Express-Cors--canberkardic.repl.co/dashboards`);
  }

  // Return an object
  getDashboard(id: number): Observable<DashboardModel> {
    return this._http.get<DashboardModel>(`https://Node-Express-Cors--canberkardic.repl.co/dashboards`);
  }

  // Update json
  updateDashboard(id: number, params): Observable<DashboardModel> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this._http.put<DashboardModel>(`https://Node-Express-Cors--canberkardic.repl.co/updateDashboard`, params, httpOptions);
  }
}
