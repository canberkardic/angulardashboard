import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { GridsterConfig, GridsterItem, GridsterItemComponentInterface } from "angular-gridster2";

import { DocumentSnapshot } from '@angular/fire/firestore';

import { DashboardService } from "../services/dashboard.service";
import { DashboardModel, DashboardContentModel } from "../../models/dashboard.model";
import { FirebaseService } from "../services/firebase.service";

// COMPONENTS
import { IToolboxComponent, ToolboxComponent } from '../components/toolbox-decorator';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls:['./dashboard.component.css' ]


})
export class DashboardComponent implements OnInit {
  constructor(private _route: ActivatedRoute,
    public _ds: DashboardService,
    public _fbs: FirebaseService
  ) {
  }

  protected options: GridsterConfig;
  protected dashboardId: number;
  protected dashboardModel: DashboardModel;
  protected componentCollection = [];

  /* To handle "undefined push property" exception we define the array empty at first. */
  protected dashboardArray: DashboardContentModel[] = [];

  ngOnInit() {
    // Grid options
		this.options = {
			gridType: "fit",
			enableEmptyCellDrop: true,
			emptyCellDropCallback: this.onDrop,
			pushItems: true,
			swap: true,
			pushDirections: { north: true, east: true, south: true, west: true },
			resizable: { enabled: true },
			itemChangeCallback: this.itemChange.bind(this),
			draggable: {
				enabled: true,
				ignoreContent: true,
				dropOverItems: true,
				dragHandleClass: "drag-handler",
				ignoreContentClass: "no-drag",
			},
			displayGrid: "always",
			minCols: 10,
			minRows: 10
		};
    this.getData();
  }

  getData() {
    // Sayfa yüklenirken önceki bileşenleri oluştur
    // We get the id in get current router dashboard/:id
let _this = this;

    this._route.params.subscribe(params => {
      // + is used to cast string to int
      this.dashboardId = +params["id"];
      // We make a get request with the dashboard id      


      this._fbs.getDashboard(this.dashboardId)
        .subscribe((userDashboard: DocumentSnapshot<any>) => {
          let dbarr: DashboardModel;

          console.log("INCOMING DATA userDashboard.data()", userDashboard.data());
          const incomingData: DashboardModel = userDashboard.data();
          console.log("INCOMING DATA ", incomingData);

          if (incomingData && !incomingData.dashboard || !Array.isArray(incomingData.dashboard))
            _this.dashboardModel = { id: 1, username: "cantek", dashboard: [] };
          else
            _this.dashboardModel = incomingData;

          _this.dashboardModel.dashboard.forEach((comp: DashboardContentModel) => {
            let foundComp = _this._ds.getToolboxWidget(comp.componentName)
            comp.component = foundComp.component;
          });

          _this.parseJson(this.dashboardModel);
          _this.dashboardArray = this.dashboardModel.dashboard.slice();
        });
    });
  }



  // Super TOKENIZER 2.0 POWERED BY NATCHOIN
  parseJson(dashboardModel: DashboardModel) {
    // We loop on our dashboardModel
    if (dashboardModel.dashboard && Array.isArray(dashboardModel.dashboard)) {
      dashboardModel.dashboard.forEach(dashboard => {
        // We loop on our componentCollection
        this.componentCollection.forEach(component => {
          // We check if component key in our dashboardModel
          // is equal to our component name key in our componentCollection
          if (dashboard.component === component.name) {
            // If it is, we replace our serialized key by our component instance
            dashboard.component = component.componentInstance;
          }
        });
      });
    }
  }

  serialize(dashboardModel) {
    // We loop on our dashboardModel
    dashboardModel.forEach(dashboard => {
      // We loop on our componentCollection
      this.componentCollection.forEach(component => {
        // We check if component key in our dashboardModel
        // is equal to our component name key in our componentCollection
        if (dashboard.name === component.name) {
          dashboard.component = component.name;
        }
      });
    });
  }

  itemChange() {
    // legacy
    this.dashboardModel.dashboard = this.dashboardArray;
    let tmp = JSON.stringify(this.dashboardModel);
    let parsed: DashboardModel = JSON.parse(tmp);
    this.serialize(parsed.dashboard);
    //this._ds.updateDashboard(this.dashboardId, parsed).subscribe();


    //firebase 
    this._fbs.updateDashboard(parsed);
  }

  onDrop(ev) {
    const componentName: String = ev.dataTransfer.getData("componentName");
    const foundCmp: IToolboxComponent = this._ds.getToolboxWidget(componentName);
    if (foundCmp) {
      var obj: any = {
        cols: 3,
        rows: 3,
        x: 0,
        y: 0,
        component: foundCmp.component,
        componentName,
        name: foundCmp.desc
      };
      //
      let tmp = JSON.stringify(obj);
      let parsed: DashboardContentModel = JSON.parse(tmp);
      this.dashboardArray.push(obj);
      this.dashboardModel.dashboard = this.dashboardArray;
      
      //this._fbs.insertWidget(parsed);
      this._fbs.upsertDashboardModel(this.dashboardModel)
    }
  }

  changedOptions() {
    this.options.api.optionsChanged();
  }

  removeItem(item) {
    this.dashboardArray.splice(
      this.dashboardArray.indexOf(item), 1
    );
    this.itemChange();
  }
}
