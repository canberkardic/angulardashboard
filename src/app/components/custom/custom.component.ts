import { Component, OnInit } from '@angular/core';
 import { ToolboxComponent } from '../toolbox-decorator';

 @ToolboxComponent({
   desc: 'Custom Component',
   icon: 'fa fa-clock',
   componentName : 'CustomComponent'
 })
@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.css']
})
export class CustomComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

}