import { Component, OnInit } from '@angular/core';
import { AlarmService } from '../../alarm.service'

export interface AlarmSettings{
  speedSettings:any[],
  alarmList:any[],
  roomList:any[]
}

@Component({
  selector: 'alarm-settings',
  templateUrl: './alarm-settings.component.html',
  styleUrls: ['./alarm-settings.component.css']
})
export class AlarmSettingsComponent implements OnInit {

  settingsData:AlarmSettings;
  constructor(private _service: AlarmService) { }

  ngOnInit() {
    this.fetchSettings();
  }

  fetchSettings() {
    this._service.fetchAlarmSettings().subscribe(
      (result:any[]):void =>{
      this.settingsData = result[0];
      
    })
  }
}
