import { DashboardService } from '../../services/dashboard.service';
import { WidgetModel, DashboardModel } from "../../../models/dashboard.model";
import { Component, OnInit, ComponentFactoryResolver, ComponentFactory } from "@angular/core";

interface CompDefinition {
  compId: string // AlarmCardComponent
  name: string // Alarm Kartı
  icon: string // "fal fa-alarm-clock"
  dragImage: string // alarm_thumb.png
}

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"]
})
export class MenuComponent implements OnInit {

  toolboxComponents: any[] = [];
  constructor(private _ds: DashboardService,
    private resolver: ComponentFactoryResolver) { };

  // Components variables
  protected toggle: boolean;
  protected modal: boolean;
  protected compCollection: CompDefinition[];
  protected dashboardCollection: DashboardModel[];

  // On component init we store Widget Marketplace in a WidgetModel array
  ngOnInit(): void {
    /* 
    this._ds.getWidgets().subscribe(widgets => {
			this.widgetCollection = widgets;
		});    
    */

    // TODO : Make it with Observable structure 
    this.toolboxComponents = this._ds.getToolboxWidgets();


    // We make get request to get all dashboards from our REST API
    this._ds.getDashboards().subscribe(dashboards => {
      this.dashboardCollection = dashboards;
    });
  }

  onDrag(event, componentName) {
    event.dataTransfer.setData('componentName', componentName);
  
  }

  toggleMenu(): void {
    this.toggle = !this.toggle;
  }
}
