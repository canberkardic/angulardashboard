export class User {
  id : number;
  name : string;
  surname : string;
  detail : Detail[];
  }

  export class Detail {
    id : number;
    alarm : string;
    company : string;
    phonenumber : string;
    mail : string;
  }

  