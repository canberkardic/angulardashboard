import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styles:[` p { font-family: Lato; } `],
  template:`
<div class="main-container">
    <app-menu></app-menu>
</div>`
})
export class AppComponent {
  
}
