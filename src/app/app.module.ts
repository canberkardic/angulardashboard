// MODULES
import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';
import { ChartsModule } from 'ng2-charts';
import { A11yModule } from '@angular/cdk/a11y';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
// Firebase configuration
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';

import { CompanyService } from './components/company-userlist/services/company.service';
import { SettingsService } from './components/company-userlist/services/settings.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FormsModule } from '@angular/forms';
import { TURKISH_DATE_FORMATS, TurkishDateFormat } from '../utils/date-format';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  
  MatFormFieldModule,
  DateAdapter,
  MAT_DATE_FORMATS,
  
} from '@angular/material';

// CSS FRAMEWORK ICONS 
import '@clr/icons';
import '@clr/icons/shapes/all-shapes';

// CONST
import { ROUTES } from './app.routes';
import {LocationProviderService}  from './components/leaflet-map/services/location-provider.service'

// COMPONENTS
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './components/menu/menu.component';

import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomComponent } from './components/custom/custom.component';
import { DashboardService } from './services/dashboard.service';
import { FirebaseService } from './services/firebase.service';
import { CompanyContainerComponent } from './components/company-userlist/company-container/company-container.component';
import { CompanyDetailsComponent } from './components/company-userlist/company-details/company-details.component';
import { CompanyListComponent } from './components/company-userlist/company-list/company-list.component';
import { CompanySettingsComponent } from './components/company-userlist/company-settings/company-settings.component';
import { BarchartComponent } from './components/barchart/barchart.component';
import { MapComponent } from './components/leaflet-map/map/map.component';
import { AlarmWidgetComponent } from './components/alarm-card/alarm-widget/alarm-widget.component';
import { AlarmCardComponent } from './components/alarm-card/alarm-widget/alarm-card/alarm-card.component';
import { AlarmSettingsComponent } from './components/alarm-card/alarm-widget/alarm-card/alarm-settings/alarm-settings.component';
import { AlarmViewComponent } from './components/alarm-card/alarm-widget/alarm-card/alarm-view/alarm-view.component';
import { UserDetailsComponent } from './components/userlist/user-details/user-details.component';
import { UserListComponent } from './components/userlist/user-list/user-list.component';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MenuComponent,

    CustomComponent,

    CompanyContainerComponent,

    CompanyDetailsComponent,

    CompanyListComponent,

    CompanySettingsComponent,

    BarchartComponent,

    MapComponent,

    AlarmWidgetComponent,

    AlarmCardComponent,

    AlarmSettingsComponent,

    AlarmViewComponent,

    UserDetailsComponent,

    UserListComponent,
  ],
  entryComponents: [CustomComponent],
  imports: [A11yModule, DragDropModule, ScrollingModule, CdkStepperModule,NgxChartsModule,
    CdkTableModule, CdkTreeModule,
    BrowserModule,
    GridsterModule,
    HttpClientModule,
    ChartsModule,
    DynamicModule.withComponents([CustomComponent,BarchartComponent,CompanyContainerComponent,MapComponent,AlarmCardComponent,UserListComponent]),
    RouterModule.forRoot(ROUTES),
    ClarityModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.config, 'replitbase'),
    AngularFireDatabaseModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule, FormsModule
  ],
  providers: [DashboardService, FirebaseService, AngularFirestore, CompanyService, SettingsService,LocationProviderService,
   { provide: DateAdapter, useClass: TurkishDateFormat },
    { provide: MAT_DATE_FORMATS, useValue: TURKISH_DATE_FORMATS }],
  bootstrap: [AppComponent]
})
export class AppModule { }
